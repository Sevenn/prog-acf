-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 29 Septembre 2014 à 16:55
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `site`
--

-- --------------------------------------------------------

--
-- Structure de la table `comptage`
--

CREATE TABLE IF NOT EXISTS `comptage` (
  `ID_Tour` int(11) NOT NULL AUTO_INCREMENT,
  `ID` int(22) NOT NULL,
  `tour` int(22) NOT NULL,
  `date` datetime(6) NOT NULL,
  PRIMARY KEY (`ID_Tour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `liste_participants`
--

CREATE TABLE IF NOT EXISTS `liste_participants` (
  `ID_Participant` int(11) NOT NULL AUTO_INCREMENT,
  `code` bigint(22) NOT NULL,
  `nom` varchar(255) COLLATE utf8_bin NOT NULL,
  `classe` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_Participant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=100 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
