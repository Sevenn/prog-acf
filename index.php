<?php
include_once('includes/connexion_sql.php');
// AJ is the best pony
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Comptage ACF</title>
	<meta name="description" content="Count system for ACF">
	<meta name="author" content="Sevenn">
	<link rel="stylesheet" href="css/5css.css">	
<style type="text/css">
</style>
</head>
<body>
<div id="header-container">
    <header class="wrapper">
        <a href="/"><img id="logo" src="images/logo.png" alt="ACF" /></a>
        <!--<nav>
            <div class="pul"><a href="#">Contact</a></div>
            <div class="pul"><a href="#">Blog</a></div>
            <div class="pul"><a href="#">Client</a></div>
            <div class="pul"><a href="#">About</a></div>
            <div class="pul"><a href="#">Home</a></div>
        </nav> todo: Bouton à faire -->
    </header>
</div>
<div id="main" class="wrapper">
    <div id="comptage">
        <ul id="tableau">
            <?php
            $resultat = $bdd->query('SELECT * FROM liste_participants p INNER JOIN comptage c ON p.ID_Participant = c.ID ORDER BY date DESC LIMIT 0, 50') or die(print_r($bdd->errorInfo())); //liaison entre les deux table + limit d'affichage de 50
            while ($resultat_tab = $resultat->fetch()) { //mise dans un tableau des résultats
                echo "<li class=\"result\">Nom : " . $resultat_tab['nom'] . "&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; Tour : " . $resultat_tab['tour'] . "</li>"; //affichage des résultat
            }
            $resultat->closeCursor(); //fermeture du curseur pour une éventuel utilisation ultérieure
            ?>
        </ul>
    </div>
    <div id="inputnum">
        <form>
            <label for="champs"><span onclick="document.getElementById('codebarre').select();">Code barre :</label>
            <input type="text" id="codebarre" class="codebarre" placeholder="Entrer code barre ici ..." size="30" />
        </form>
    </div>
</div>
<div id="footer-container">
    <footer class="wrapper">
        <div class="copy"><a href="https://fr.gravatar.com/sevenn83" target="_blank">Coder &amp; Design Sevenn</a> for <a href="http://www.actioncontrelafaim.org/" target="_blank">ACF</a> &copy; copyrigth Chensocraft.net</div>
    </footer>
</div>
</body>
</html>
