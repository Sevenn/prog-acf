<?php

include_once('config.php');

try
{
    $bdd = new PDO(sprintf('mysql:hostname=%s;dbname=%s', $hote, $base), $login, $mdp);
}
catch(Expetion $e)
{
    die('Erreur : ' . $e->getMessage());
}
